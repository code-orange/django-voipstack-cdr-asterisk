from django.db import models


class Cdr(models.Model):
    id = models.BigAutoField(primary_key=True)
    accountcode = models.CharField(max_length=20, blank=True, null=True)
    src = models.CharField(max_length=80, blank=True, null=True)
    dst = models.CharField(max_length=80, blank=True, null=True)
    dcontext = models.CharField(max_length=80, blank=True, null=True)
    clid = models.CharField(max_length=80, blank=True, null=True)
    channel = models.CharField(max_length=80, blank=True, null=True)
    dstchannel = models.CharField(max_length=80, blank=True, null=True)
    lastapp = models.CharField(max_length=80, blank=True, null=True)
    lastdata = models.CharField(max_length=80, blank=True, null=True)
    start = models.DateTimeField(blank=True, null=True)
    answer = models.DateTimeField(blank=True, null=True)
    end = models.DateTimeField(blank=True, null=True)
    duration = models.IntegerField(blank=True, null=True)
    billsec = models.IntegerField(blank=True, null=True)
    disposition = models.CharField(max_length=45, blank=True, null=True)
    amaflags = models.CharField(max_length=45, blank=True, null=True)
    userfield = models.CharField(max_length=256, blank=True, null=True)
    uniqueid = models.CharField(max_length=150, blank=True, null=True)
    linkedid = models.CharField(max_length=150, blank=True, null=True)
    peeraccount = models.CharField(max_length=20, blank=True, null=True)
    sequence = models.IntegerField(blank=True, null=True)

    def get_cel(self):
        cel_queryset = Cel.objects.filter(
            uniqueid=self.uniqueid,
            linkedid=self.linkedid,
        )

        return cel_queryset

    class Meta:
        managed = False
        db_table = "cdr"


class Cel(models.Model):
    id = models.BigAutoField(primary_key=True)
    eventtype = models.CharField(max_length=30)
    eventtime = models.DateTimeField()
    cid_name = models.CharField(max_length=80)
    cid_num = models.CharField(max_length=80)
    cid_ani = models.CharField(max_length=80)
    cid_rdnis = models.CharField(max_length=80)
    cid_dnid = models.CharField(max_length=80)
    exten = models.CharField(max_length=80)
    context = models.CharField(max_length=80)
    channame = models.CharField(max_length=80)
    src = models.CharField(max_length=80)
    dst = models.CharField(max_length=80)
    channel = models.CharField(max_length=80)
    dstchannel = models.CharField(max_length=80)
    appname = models.CharField(max_length=80)
    appdata = models.CharField(max_length=80)
    amaflags = models.IntegerField()
    accountcode = models.CharField(max_length=20)
    uniqueid = models.CharField(max_length=32)
    linkedid = models.CharField(max_length=32)
    peer = models.CharField(max_length=80)
    userdeftype = models.CharField(max_length=255)
    eventextra = models.CharField(max_length=255)
    userfield = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = "cel"

from datetime import datetime

from django.db import models


class VoipCdrAsteriskDb(models.Model):
    name = models.CharField(max_length=200, unique=True)
    db_ip = models.CharField(max_length=200)
    db_dbase = models.CharField(max_length=200)
    db_user = models.CharField(max_length=200)
    db_passwd = models.CharField(max_length=200)
    date_added = models.DateTimeField(default=datetime.now)
    last_successful = models.DateTimeField(default=datetime.now)

    class Meta:
        db_table = "voip_cdr_asterisk_db"
